package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ReadDataForChecking {

    private ArrayList<String> filenamecheck = new ArrayList<String>();

    Connection connection = new DatabaseConnect().obtainConnection();

    public ArrayList<String> readdataforcheck() throws SQLException {
        String query2 = "select filename from txtfiledetails";
        Statement st = null;
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery(query2);
            while (rs.next()) {

                filenamecheck.add(rs.getString(1));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }


        return filenamecheck;

    }


}

