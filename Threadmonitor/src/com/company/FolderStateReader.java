package com.company;

import java.io.*;

import java.io.File;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class FolderStateReader implements Runnable {

    BufferedReader br = null;
    FileReader fr = null;
    WritetoDb writetodb = new WritetoDb();
    ReadDataForChecking readDataForChecking = new ReadDataForChecking();

    private ArrayList<String> currentFileListInDataSource;

    boolean loopRun=false;


    public FolderStateReader() {
        try {
            currentFileListInDataSource = readDataForChecking.readdataforcheck();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {


        File file = new File("/home/training/jis");

        while (!loopRun) {
            File[] files = file.listFiles();

            for (File f : files) {
                if (f.isFile()) {
                    if (!currentFileListInDataSource.contains(f.getName())) {
                        currentFileListInDataSource.add(f.getName());


                        try {
                            int size = (int) f.length();
                            fr = new FileReader(f);
                            br = new BufferedReader(fr);
                            String nextLine = null;
                            StringBuilder sb = new StringBuilder();
                            while ((nextLine = br.readLine()) != null) {
                                sb.append(nextLine);
                            }
                            String tr = String.valueOf(Calendar.getInstance().getTime());
                            System.out.println("" + tr);
                            writetodb.writeToDb(f.getName(), size, sb.toString(), tr);


                        } catch (FileNotFoundException e) {
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }

            }
        }
    }
}

